## Configuración inicial

3. Instala las extenciones adicionales de php necesarias

```
$ sudo apt update
$ sudo apt install php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip composer
```

-  Revisa la versión que instalaste de php

```
$ php -v
```

-  Reinicia los servicios de php-fpm correspondiente a la versión instalada (7.2, 7.4 o superior).

```
$ sudo systemctl restart php7.4-fpm
```

4. Edita el server block del sitio de la siguiente manera.

```
$ cd /etc/nginx/sites-available/
$ sudo cp default mysite.com
$ sudo vim mysite.com
```

-  Editamos la configuración del host virtual.

```
server {
        listen 80;
        root /var/www/html/web;
        index index.php index.html index.htm index.nginx-debian.html;
        server_name mysite.com www.mysite.com;

        location = /favicon.ico { log_not_found off; access_log off; }

        location ~* \.(css|gif|ico|jpeg|jpg|js|png)$ {
                expires max;
                log_not_found off;
        }

        location / {
                #try_files $uri $uri/ =404;
                try_files $uri $uri/ /index.php$is_args$args;
        }

        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                # CAMBIA LA VERSION DE PHP A LA QUE INSTALASTE
                fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        }

        location ~ /\.ht {
                deny all;
        }
}
```

-  Crear link simbólico en el servidor.

```
$ cd /etc/nginx/sites-enabled/
$ ln -s /etc/nginx/sites-available/mysite.com
```

-  Revisa que el link simbólico se haya creado correctamente con el siguiente comando.

```
$ ll
```

-  **Ejecuta los siguientes comandos para reiniciar Nginx**

```
$ sudo nginx -t
$ sudo systemctl reload nginx
```

# Clonar el repositorio

10. Crea el archivo `.env` cambia los permisos y editalo con la información del proyecto.

```
$ sudo cp /var/www/html/.env.example /var/www/html/.env
$ sudo vim /var/www/html/.env
$ sudo chmod 774 .env
```

# Instalación de craft

1. Instalamos craft desde la terminal.

```
$ cd /var/www/html/
$ ./craft setup
```

-  Si no te permite instalar craft, intenta con el siguiente comando. **Saltate este paso si ya lograste instarlo**

```
$ php craft setup
```

> ## ¡Craft instalado! 🎉🎊
>
> ### Hasta este momento ya deberías de tener craft instalado y funcionando. Accede a la dirección IP de tu droplet para comprobar.

-  Si después de comprobar la instalación de craft en tu droplet te encuentras con un error en la carpeta `storage/` basta con cambiar los permisos.

```
$ sudo chmod -R 777 * storage/
```

> ### Si tienes problemas con el **deployment**, acercate con cualquier miembro del team para que te apoye 🤝
