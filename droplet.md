# Obtener archivos que no están en GitLab (CraftCMS)

## Introducción

Este documento tiene como proposito guiar en la descarga e instalación de proyectos desarrollados con **CraftCMS** que no tienen historial en GitLab.

## Requisitos importantes

-  Asegurate de que el proyecto con el que vas a trabajar no exista en el workflow de Brand Industry y que ninguno de los compañeros desarrolladores lo tenga en privado.
-  Tener acceso al droplet con el que vamos a trabajar.

## Descargar archivos y compartirlos en GitLab

1. Ingresamos al droplet por medio de SSH.

```
$ ssh user@host
```

2. Asegurate de que craft esta instalado en la ruta:

```
/var/www/html/
```

> Si Craft no se encuentra instalado en la ruta pasada, navega sobre la carpeta /www para encontrar en donde fue instalado.

3. Descargaremos el proyecto que esta en el producción a nuestro entorno local. Ejecutamos el siguiente comando:

```
$ scp -r user@host:/var/www/html/ ./Sites
```

> El primer parametro se refiere a la ruta en donde esta instalado Craft en el droplet. En el segundo parametro pasamos la ruta en la que generalmente instalamos Craft en nuestro entorno local.

4. Reenombra la carpeta `html/` al proyecto con el que trabajas y muevete.

5. Crea un repositorio en GitLab con el nombre del proyecto en el workflow de Brand Industry.

6. Inicia el proyecto con **git** y agrega el repositorio que creaste.

```
git init
git remote add origin <remote repository>
```

7. Crea una rama con el nombre **stable-version** y haz el primer commit.

```
git checkout -b stable-version
git add -A
git commit -m "Initial commit"
```

8. Crea un archivo `README.md` en el repositorio desde GitLab, con la idea de que se registre la rama **master**.

9. Haz push hacia el repositorio creado.

```
git push origin stable-version
```

10. Entra al repositorio ejecuta un **merge-request** hacia master con los cambios que subiste.

> ### Archivos compartidos 🎉
>
> Hasta este punto ya tienes cargados los archivos que estaban en el droplet y ahora todo el equipo tienen acceso.

## Instalar proyecto en local

> ### Esta sección solo aplica si vas a subir nuevos cambios al droplet.

Antes de instalar Craft, es necesario moverse a la rama master y hacer fetch para obtener los commit que tenemos en el repositorio.

```
git checkout master
git fetch origin master
git pull origin master
```

Ahora que ya tienes los commits actualizados, crea una rama con el nombre de los cambios que vas a hacer.

```
git checkout -b webcare
```

Continúa con la guia de instalación en el **paso 4** del siguiente enlace [Consultar guia de instalación.](https://docs.thebrandindustry.com/craft-cms/local-development.html#clonar-repositorio)

Haz los cambios, subelos al repositorio y ejecuta el merge-request.

## Ejecutar los cambios al droplet

En esta sección nos enfocaremos en subir los cambios que tenemos en el repositorio al servidor.

1. Nos conectamos con SSH

```
$ ssh user@host
```

2. Copia la llave y agrégala a gitlab. Dirigete a la siguiente sección y **sigue los pasos 1, 2, 3, 4, 5, 6, 7, 8, 9 y 11**. [Consultar seccion.](https://docs.thebrandindustry.com/craft-cms/deployment.html#clonar-el-repositorio)

3. Automatizar el deploy. [Consultar seccion.](https://docs.thebrandindustry.com/craft-cms/deployment.html#automatizar-el-deploy)

## ¡Droplet actualizado! 🎉🎊

**Hasta este punto ya deberías de ver los cambios que hiciste en el servidor.**

**Si tienes problemas durante el proceso, acercate con cualquier miembro del equipo para que te apoye** 🤝
